#!/bin/bash

# Check if start date, end date, and granularity are provided as arguments
if [ $# -ne 3 ]; then
    echo "Usage: $0 [start-date] [end-date] [granularity]"
    echo "Example: $0 2022-01-01 2022-01-31 MONTHLY"
    exit 1
fi

START_DATE="$1"
END_DATE="$2"
GRANULARITY="$3"

# Validate granularity input
if [[ "$GRANULARITY" != "DAILY" && "$GRANULARITY" != "MONTHLY" && "$GRANULARITY" != "HOURLY" ]]; then
    echo "Invalid granularity. Allowed values are DAILY, MONTHLY, or HOURLY."
    exit 1
fi

# Check if jq is installed
if ! command -v jq &> /dev/null; then
    echo "jq is required but not installed. Please install jq and try again."
    exit 1
fi

# AWS CLI command to get detailed billing data by service and region
response=$(aws ce get-cost-and-usage \
    --time-period Start="$START_DATE",End="$END_DATE" \
    --granularity "$GRANULARITY" \
    --metrics "BlendedCost" \
    --group-by Type=DIMENSION,Key=SERVICE Type=DIMENSION,Key=REGION)

# Check if the AWS CLI command was successful
if [ $? -ne 0 ]; then
    echo "Failed to retrieve detailed billing data. Please check your AWS CLI configuration and try again."
    exit 1
fi

# Print headers
printf "+-------------------------------------------+-----------------+---------------+\n"
printf "| %-40s | %-15s | %-13s |\n" "Service" "Region" "BlendedCost"
printf "+-------------------------------------------+-----------------+---------------+\n"

# Variables for calculating total cost
totalCost=0

# Parse and format the data using jq and awk
echo "$response" | jq -r '
    .ResultsByTime[] |
    .Groups[] |
    [.Keys[0], .Keys[1], (.Metrics.BlendedCost.Amount | tonumber // 0)] |
    @tsv' | awk -F'\t' '{
        service = $1
        region = $2
        blendedCost = ($3 != "" ? $3 : 0)
        totalCost += blendedCost
        printf "| %-40s | %-15s | %-13.2f |\n", service, region, blendedCost
    } END {
        printf "+-------------------------------------------+-----------------+---------------+\n"
        printf "| %-56s | %-13.2f |\n", "Total Cost", totalCost
        printf "+-------------------------------------------+-----------------+---------------+\n"
    }'
