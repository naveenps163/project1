#!/bin/bash

# Check if AWS region is provided as a command-line argument
if [ -z "$1" ]; then
    echo "Usage: $0 <aws-region>"
    exit 1
fi

# Set AWS region from command-line argument
AWS_REGION="$1"

# Configure AWS CLI with the specified region
export AWS_DEFAULT_REGION="$AWS_REGION"

# List IAM users
echo "IAM Users:"
aws iam list-users --output table
