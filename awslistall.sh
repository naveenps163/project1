#!/bin/bash

# Check if AWS region is provided as a command-line argument
if [ -z "$1" ]; then
    echo "Usage: $0 <aws-region>"
    exit 1
fi

# Set AWS region from command-line argument
AWS_REGION="$1"

# Function to print section headers
print_header() {
    echo -e "\n$1:"
}

# List EC2 instances and get their resource usage
print_header "EC2 Instances"
aws ec2 describe-instances --region $AWS_REGION --query 'Reservations[*].Instances[*].[InstanceId, InstanceType, State.Name, PrivateIpAddress, PublicIpAddress]' --output table

# List AMIs
print_header "Amazon Machine Images (AMIs)"
aws ec2 describe-images --owners self --region $AWS_REGION --query 'Images[*].[ImageId, Name, State, CreationDate]' --output table

# List RDS instances and get their resource usage
print_header "RDS Instances"
aws rds describe-db-instances --region $AWS_REGION --query 'DBInstances[*].[DBInstanceIdentifier, DBInstanceClass, DBInstanceStatus, Engine, EngineVersion, Endpoint.Address]' --output table

# List S3 buckets
print_header "S3 Buckets"
aws s3 ls --output table

# List Lambda functions and get their resource usage
print_header "Lambda Functions"
aws lambda list-functions --region $AWS_REGION --query 'Functions[*].[FunctionName, Runtime, MemorySize, Timeout]' --output table

# List DynamoDB tables
print_header "DynamoDB Tables"
aws dynamodb list-tables --region $AWS_REGION --query 'TableNames' --output table

# List CloudFormation stacks
print_header "CloudFormation Stacks"
aws cloudformation describe-stacks --region $AWS_REGION --query 'Stacks[*].[StackName, StackStatus]' --output table

# List Elastic Beanstalk applications
print_header "Elastic Beanstalk Applications"
aws elasticbeanstalk describe-applications --region $AWS_REGION --query 'Applications[*].[ApplicationName, Status]' --output table

# List IAM users
print_header "IAM Users"
aws iam list-users --query 'Users[*].[UserName, UserId, Arn]' --output table

# List EKS clusters
print_header "EKS Clusters"
aws eks list-clusters --region $AWS_REGION --query 'clusters' --output table

# List ECS clusters
print_header "ECS Clusters"
aws ecs list-clusters --region $AWS_REGION --query 'clusterArns' --output table

# List SNS topics
print_header "SNS Topics"
aws sns list-topics --region $AWS_REGION --query 'Topics[*].[TopicArn]' --output table

# List CloudTrail trails
print_header "CloudTrail Trails"
aws cloudtrail describe-trails --region $AWS_REGION --query 'trailList[*].[Name, HomeRegion]' --output table

# List VPC peering connections
print_header "VPC Peering Connections"
aws ec2 describe-vpc-peering-connections --region $AWS_REGION --query 'VpcPeeringConnections[*].[VpcPeeringConnectionId, Status.Code]' --output table

# List DataSync tasks
print_header "DataSync Tasks"
aws datasync list-tasks --region $AWS_REGION --query 'Tasks[*].[TaskArn, Status]' --output table

# List Direct Connect connections
print_header "Direct Connect Connections"
aws directconnect describe-connections --region $AWS_REGION --query 'connections[*].[connectionId, connectionState]' --output table

# List CloudWatch alarms
print_header "CloudWatch Alarms"
aws cloudwatch describe-alarms --region $AWS_REGION --query 'MetricAlarms[*].[AlarmName, StateValue]' --output table

# List EFS file systems
print_header "EFS File Systems"
aws efs describe-file-systems --region $AWS_REGION --query 'FileSystems[*].[FileSystemId, CreationTime, LifeCycleState]' --output table

# List Route 53 hosted zones
print_header "Route 53 Hosted Zones"
aws route53 list-hosted-zones --query 'HostedZones[*].[Name, Id]' --output table

# List Auto Scaling groups
print_header "Auto Scaling Groups"
aws autoscaling describe-auto-scaling-groups --region $AWS_REGION --query 'AutoScalingGroups[*].[AutoScalingGroupName, MinSize, MaxSize, DesiredCapacity]' --output table

# List ACM certificates
print_header "ACM Certificates"
aws acm list-certificates --region $AWS_REGION --query 'CertificateSummaryList[*].[DomainName, CertificateArn]' --output table

# List NAT gateways
print_header "NAT Gateways"
aws ec2 describe-nat-gateways --region $AWS_REGION --query 'NatGateways[*].[NatGatewayId, State]' --output table

# List Amazon ECR repositories
print_header "Amazon ECR Repositories"
aws ecr describe-repositories --region $AWS_REGION --query 'repositories[*].[repositoryName, repositoryUri]' --output table

# List Amazon CloudFront distributions
print_header "CloudFront Distributions"
aws cloudfront list-distributions --query 'DistributionList.Items[*].[Id, DomainName]' --output table

# List AWS AppSync APIs
print_header "AWS AppSync APIs"
aws appsync list-graphql-apis --region $AWS_REGION --query 'graphqlApis[*].[apiId, apiName]' --output table

# List AWS Amplify apps
print_header "AWS Amplify Apps"
aws amplify list-apps --region $AWS_REGION --query 'apps[*].[appId, name]' --output table

# List AWS Key Management Service (KMS) keys
print_header "AWS KMS Keys"
aws kms list-keys --region $AWS_REGION --query 'Keys[*].[KeyId, KeyArn]' --output table

# List AWS Secrets Manager secrets
print_header "AWS Secrets Manager Secrets"
aws secretsmanager list-secrets --region $AWS_REGION --query 'SecretList[*].[Name, ARN]' --output table

# List Elastic Load Balancers (ELB, ALB, NLB)
print_header "Elastic Load Balancers"
aws elbv2 describe-load-balancers --region $AWS_REGION --query 'LoadBalancers[*].[LoadBalancerName, DNSName, State.Code, Type, Scheme]' --output table

# List Elastic IPs
print_header "Elastic IPs"
aws ec2 describe-addresses --region $AWS_REGION --query 'Addresses[*].[PublicIp, AllocationId, InstanceId, NetworkInterfaceId]' --output table

# List Internet Gateways (IWG)
print_header "Internet Gateways"
aws ec2 describe-internet-gateways --region $AWS_REGION --query 'InternetGateways[*].[InternetGatewayId, Attachments[*].VpcId]' --output table

# List Network ACLs (NACL)
print_header "Network ACLs"
aws ec2 describe-network-acls --region $AWS_REGION --query 'NetworkAcls[*].[NetworkAclId, VpcId, IsDefault]' --output table

# List Transit Gateways
print_header "Transit Gateways"
aws ec2 describe-transit-gateways --region $AWS_REGION --query 'TransitGateways[*].[TransitGatewayId, State, Description]' --output table

# List API Gateways
print_header "API Gateways"
aws apigateway get-rest-apis --region $AWS_REGION --query 'items[*].[id, name, description]' --output table
