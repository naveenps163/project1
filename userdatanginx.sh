#!/bin/bash

# Update package repository
sudo yum update -y

# Install Nginx
sudo amazon-linux-extras install nginx1.12 -y

# Start Nginx service
sudo systemctl start nginx

# Enable Nginx to start on boot
sudo systemctl enable nginx

# Check Nginx status
sudo systemctl status nginx

