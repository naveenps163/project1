# To install rancer on EKS cluster 
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
kubectl create namespace cattle-system
helm install rancher rancher-stable/rancher --namespace cattle-system --set hostname=my-rancher-domain.com


kubectl get svc -n cattle-system
kubectl edit svc -n cattle-system ( change cluster to LoadBalancer)

http://<hostname-or-ip>:<port>

kubectl config set-context <context-name> --namespace=<new-namespace> ( change default namespace to cattle-system)



