#!/bin/bash

# Function to display usage information
usage() {
    echo "Usage: $0 -r <region>"
    exit 1
}

# Function to list CloudFormation stacks
list_stacks() {
    echo "Listing CloudFormation stacks in region $REGION..."
    aws cloudformation --region "$REGION" describe-stacks --query 'Stacks[*].[StackName]' --output text
}

# Function to delete a CloudFormation stack
delete_stack() {
    echo "Enter the name of the stack you want to delete:"
    read -r stack_name
    echo "Deleting CloudFormation stack $stack_name in region $REGION..."
    aws cloudformation --region "$REGION" delete-stack --stack-name "$stack_name"

    # Wait for the stack to be deleted
    echo "Waiting for CloudFormation stack $stack_name to be deleted..."
    aws cloudformation --region "$REGION" wait stack-delete-complete --stack-name "$stack_name"

    echo "CloudFormation stack $stack_name deletion completed."
}

# Parse command-line options
while getopts "r:" opt; do
    case $opt in
        r)
            REGION=$OPTARG
            ;;
        *)
            usage
            ;;
    esac
done

# Verify required options are provided
if [[ -z $REGION ]]; then
    usage
fi

# Display menu options
while true; do
    echo "Please choose an option:"
    echo "1. List CloudFormation stacks"
    echo "2. Delete a CloudFormation stack"
    echo "3. Exit"

    read -rp "Enter your choice: " choice

    case $choice in
        1)
            list_stacks
            ;;
        2)
            delete_stack
            ;;
        3)
            echo "Exiting..."
            exit 0
            ;;
        *)
            echo "Invalid option. Please choose again."
            ;;
    esac
done
