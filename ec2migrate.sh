#!/bin/bash

# Function to list EC2 instances based on region
list_ec2_instances() {
    local region=$1
    echo "Listing EC2 instances in region $region..."
    aws ec2 describe-instances --region $region --query 'Reservations[*].Instances[*].[InstanceId, InstanceType, State.Name, PublicIpAddress, PrivateIpAddress]' --output table
}

# Function to create an image of an EC2 instance
create_ec2_image() {
    read -p "Enter the ID of the instance to create an image from: " instance_id
    read -p "Enter a name for the new image: " image_name

    echo "Creating image of EC2 instance $instance_id..."
    image_id=$(aws ec2 create-image --instance-id $instance_id --name "$image_name-$(date +%Y-%m-%d-%H-%M-%S)" --output text)

    # Wait for the image creation to complete
    echo "Waiting for image creation to complete..."
    aws ec2 wait image-available --image-ids $image_id

    echo "Image $image_id created successfully."
}

# Function to list available AMIs based on region
list_amis() {
    local region=$1
    echo "Listing available AMIs in region $region..."
    aws ec2 describe-images --region $region --owners self --query 'Images[*].[ImageId, Name, CreationDate]' --output table
}

# Function to launch a new EC2 instance using a specified AMI
launch_ec2_instance() {
    read -p "Enter the ID of the AMI to use for launching the new instance: " ami_id
    read -p "Enter the instance type: " instance_type
    read -p "Enter the key pair name: " key_name
    read -p "Enter the subnet ID: " subnet_id
    read -p "Enter the region for launching the instance: " region

    echo "Launching new EC2 instance using AMI $ami_id in region $region..."
    aws ec2 run-instances --region $region --image-id $ami_id --instance-type $instance_type --key-name $key_name --subnet-id $subnet_id

    echo "New EC2 instance launched successfully."
}

# Function to migrate an image to another region
migrate_image() {
    read -p "Enter the ID of the image to migrate: " image_id
    read -p "Enter the destination region: " destination_region

    echo "Migrating image $image_id to region $destination_region..."
    destination_image_id=$(aws ec2 copy-image --source-image-id $image_id --source-region $region --region $destination_region --name "MigratedImage-$(date +%Y-%m-%d-%H-%M-%S)" --output text)

    # Wait for the image migration to complete
    echo "Waiting for image migration to complete..."
    aws ec2 wait image-available --region $destination_region --image-ids $destination_image_id

    echo "Image $image_id migrated successfully to region $destination_region with new image ID: $destination_image_id"
}

# Main menu
while true; do
    echo "1. List EC2 instances based on region"
    echo "2. Create an image of an EC2 instance"
    echo "3. List available AMIs based on region"
    echo "4. Launch a new EC2 instance using an AMI"
    echo "5. Migrate an image to another region"
    echo "6. Exit"
    read -p "Select an option: " choice

    case $choice in
        1) read -p "Enter the region to list EC2 instances: " region; list_ec2_instances $region;;
        2) create_ec2_image;;
        3) read -p "Enter the region to list available AMIs: " region; list_amis $region;;
        4) launch_ec2_instance;;
        5) migrate_image;;
        6) exit;;
        *) echo "Invalid option. Please try again.";;
    esac
done
