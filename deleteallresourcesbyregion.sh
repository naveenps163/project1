#!/bin/bash

# Set the AWS region
AWS_REGION="your-region"

# Delete EC2 instances
aws ec2 terminate-instances --region "$AWS_REGION" --instance-ids instance-id-1 instance-id-2 ...

# Delete S3 buckets
#aws s3 rb s3://bucket-name-1 --region "$AWS_REGION" --force
#aws s3 rb s3://bucket-name-2 --region "$AWS_REGION" --force


# Delete EKS clusters
aws eks delete-cluster --region "$AWS_REGION" --name cluster-name-1
aws eks delete-cluster --region "$AWS_REGION" --name cluster-name-2


# Delete key pairs
aws ec2 delete-key-pair --region "$AWS_REGION" --key-name key-pair-name-1
aws ec2 delete-key-pair --region "$AWS_REGION" --key-name key-pair-name-2


# Delete NAT gateways
aws ec2 delete-nat-gateway --region "$AWS_REGION" --nat-gateway-id nat-gateway-id-1
aws ec2 delete-nat-gateway --region "$AWS_REGION" --nat-gateway-id nat-gateway-id-2


# Delete VPC peering connections
aws ec2 delete-vpc-peering-connection --region "$AWS_REGION" --vpc-peering-connection-id peering-connection-id-1
aws ec2 delete-vpc-peering-connection --region "$AWS_REGION" --vpc-peering-connection-id peering-connection-id-2

