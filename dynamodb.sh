#!/bin/bash

# Function to list all DynamoDB tables
list_dynamodb_tables() {
    local region="$1"

    echo "Listing all DynamoDB tables in region '$region':"
    aws dynamodb list-tables --region "$region" --query 'TableNames'
}

# Function to create a DynamoDB table
create_dynamodb_table() {
    local region="$1"
    local table_name="$2"

    aws dynamodb create-table \
        --region "$region" \
        --table-name "$table_name" \
        --attribute-definitions \
            AttributeName=ID,AttributeType=N \
        --key-schema \
            AttributeName=ID,KeyType=HASH \
        --provisioned-throughput \
            ReadCapacityUnits=5,WriteCapacityUnits=5

    echo "DynamoDB table '$table_name' created in region '$region'."
}

# Function to delete a DynamoDB table
delete_dynamodb_table() {
    local region="$1"
    local table_name="$2"

    aws dynamodb delete-table --region "$region" --table-name "$table_name"

    echo "DynamoDB table '$table_name' deleted from region '$region'."
}

# Main function
main() {
    local region="$1"

    while true; do
        echo "Menu:"
        echo "1) List DynamoDB tables"
        echo "2) Create DynamoDB table"
        echo "3) Delete DynamoDB table"
        echo "4) Exit"

        read -p "Select an option: " choice

        case "$choice" in
            1)
                list_dynamodb_tables "$region"
                ;;
            2)
                read -p "Enter the name of the table to create: " table_name
                create_dynamodb_table "$region" "$table_name"
                ;;
            3)
                read -p "Enter the name of the table to delete: " table_name
                delete_dynamodb_table "$region" "$table_name"
                ;;
            4)
                echo "Exiting."
                exit 0
                ;;
            *)
                echo "Invalid option. Please select again."
                ;;
        esac
    done
}

# Check if required arguments are provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <region>"
    exit 1
fi

# Read input arguments
REGION="$1"

# Run the main function
main "$REGION"
