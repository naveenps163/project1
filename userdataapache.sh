#!/bin/bash

# Update package repository
sudo yum update -y

# Install Apache HTTP Server
sudo yum install httpd -y

# Start Apache service
sudo systemctl start httpd

# Enable Apache to start on boot
sudo systemctl enable httpd

# Check Apache status
sudo systemctl

