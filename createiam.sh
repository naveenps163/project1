#!/bin/bash

# Check if AWS region is provided as a command-line argument
if [ -z "$1" ]; then
    echo "Usage: $0 <aws-region>"
    exit 1
fi

# Set AWS region from command-line argument
AWS_REGION="$1"

# Create IAM user
echo "Creating IAM user..."
user_name="example-user"
aws iam create-user --user-name "$user_name" --region "$AWS_REGION"

# Generate access key and secret key for the user
echo "Generating access key and secret key..."
credentials=$(aws iam create-access-key --user-name "$user_name" --output json --region "$AWS_REGION")

# Extract access key ID and secret access key from the response
access_key_id=$(echo "$credentials" | jq -r '.AccessKey.AccessKeyId')
secret_access_key=$(echo "$credentials" | jq -r '.AccessKey.SecretAccessKey')

# Display access key ID and secret access key
echo "Access Key ID: $access_key_id"
echo "Secret Access Key: $secret_access_key"
