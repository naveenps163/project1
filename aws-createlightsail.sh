#!/bin/bash

# Check if region, bundle ID, and blueprint ID arguments are provided
if [ $# -ne 4 ]; then
    echo "Usage: $0 <region> <bundle-id> <blueprint-id> <instance-name>"
    exit 1
fi

region="$1"
bundle_id="$2"
blueprint_id="$3"
instance_name="$4"

# Create Lightsail instance
aws lightsail create-instances \
    --instance-names "$instance_name" \
    --availability-zone "${region}a" \
    --bundle-id "$bundle_id" \
    --blueprint-id "$blueprint_id" \
    --output text \
    --region "$region"

echo "Lightsail instance created in region $region with name $instance_name."
