#!/bin/bash

# Get a list of AWS regions
regions=$(aws ec2 describe-regions --output text --query 'Regions[*].RegionName')

# Loop through each region
for region in $regions; do
    echo "Region: $region"
    
    # List S3 buckets in the region
    echo "S3 Buckets:"
    aws s3api list-buckets --region "$region" --query 'Buckets[].Name'
    echo ""
    
    # List EKS clusters in the region
    echo "EKS Clusters:"
    aws eks list-clusters --region "$region" --query 'clusters'
    echo ""
    
    # Describe NAT gateways in the region
    echo "NAT Gateways:"
    aws ec2 describe-nat-gateways --region "$region" --query 'NatGateways[].NatGatewayId'
    echo ""
    
    # Describe VPC peering connections in the region
    echo "VPC Peering Connections:"
    aws ec2 describe-vpc-peering-connections --region "$region" --query 'VpcPeeringConnections[].VpcPeeringConnectionId'
    echo ""

    # Describe key pairs in the region and count them
    echo "Key Pairs:"
    key_pair_count=$(aws ec2 describe-key-pairs --region "$region" --query 'length(KeyPairs[])')
    echo "Number of key pairs: $key_pair_count"
    echo ""
    
    # Describe EC2 instances in the region
    echo "EC2 Instances:"
    aws ec2 describe-instances --region "$region" --query 'Reservations[].Instances[].InstanceId'
    echo ""
done
