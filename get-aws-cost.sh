#!/bin/bash

# Check if both start date, end date, and granularity are provided as arguments
if [ $# -ne 3 ]; then
    echo "Usage: $0 [start-date] [end-date] [granularity]"
    echo "Example: $0 2022-01-01 2022-01-31 MONTHLY"
    exit 1
fi

START_DATE="$1"
END_DATE="$2"
GRANULARITY="$3"

# Get the cost and usage for the specified time range
echo "Retrieving cost and usage for the period: $START_DATE to $END_DATE with granularity: $GRANULARITY"
aws ce get-cost-and-usage \
    --time-period Start="$START_DATE",End="$END_DATE" \
    --granularity "$GRANULARITY" \
    --metrics "BlendedCost" "UnblendedCost" "UsageQuantity"
