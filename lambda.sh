#!/bin/bash

# Function to list all Lambda functions
list_lambda_functions() {
    echo "Listing all Lambda functions:"
    aws lambda list-functions --query 'Functions[*].[FunctionName]' --output table
}

# Function to list all Lambda function handlers
list_lambda_handlers() {
    echo "Listing all Lambda function handlers:"
    aws lambda list-functions --query 'Functions[*].[Handler]' --output table
}

# Function to list all IAM role ARNs
list_role_arns() {
    echo "Listing all IAM role ARNs:"
    aws iam list-roles --query 'Roles[*].[Arn]' --output table
}

# Function to list all Lambda function runtimes
list_lambda_runtimes() {
    echo "Listing all Lambda function runtimes:"
    aws lambda list-functions --query 'Functions[*].[Runtime]' --output table
}

# Function to create an AWS Lambda function
create_lambda_function() {
    local function_name="$1"
    local handler="$2"
    local role_arn="$3"
    local runtime="$4"
    local zip_file="$5"

    echo "Creating Lambda function '$function_name'..."
    aws lambda create-function \
        --function-name "$function_name" \
        --handler "$handler" \
        --role "$role_arn" \
        --runtime "$runtime" \
        --zip-file "fileb://$zip_file"
    
    echo "Lambda function '$function_name' created successfully."
}

# Function to delete a Lambda function
delete_lambda_function() {
    local function_name="$1"

    echo "Deleting Lambda function '$function_name'..."
    aws lambda delete-function --function-name "$function_name"
    
    echo "Lambda function '$function_name' deleted successfully."
}

# Main function
main() {
    while true; do
        echo "Menu:"
        echo "1) List Lambda functions"
        echo "2) List Lambda function handlers"
        echo "3) List IAM role ARNs"
        echo "4) List Lambda function runtimes"
        echo "5) Create Lambda function"
        echo "6) Delete Lambda function"
        echo "7) Exit"

        read -p "Select an option: " choice

        case "$choice" in
            1)
                list_lambda_functions
                ;;
            2)
                list_lambda_handlers
                ;;
            3)
                list_role_arns
                ;;
            4)
                list_lambda_runtimes
                ;;
            5)
                read -p "Enter the name of the function: " function_name
                read -p "Enter the handler name: " handler
                read -p "Enter the ARN of the IAM role: " role_arn
                read -p "Enter the runtime environment (e.g., nodejs12.x): " runtime
                read -p "Enter the path to the ZIP file containing the function code: " zip_file
                create_lambda_function "$function_name" "$handler" "$role_arn" "$runtime" "$zip_file"
                ;;
            6)
                read -p "Enter the name of the function to delete: " function_name
                delete_lambda_function "$function_name"
                ;;
            7)
                echo "Exiting."
                exit 0
                ;;
            *)
                echo "Invalid option. Please select again."
                ;;
        esac
    done
}

# Run the main function
main
