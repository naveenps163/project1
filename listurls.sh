#!/bin/bash

# Function to list SSH URLs of EC2 instances
list_ssh_urls() {
    instances=$(aws ec2 describe-instances --region $1 --query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text)
    echo "SSH URLs for EC2 instances in region $1:"
    while read -r ip; do
        echo "ssh ec2-user@$ip"
    done <<< "$instances"
}

# Main script
if [ -z "$1" ]; then
    echo "Usage: $0 <region>"
    exit 1
fi

AWS_REGION=$1

list_ssh_urls $AWS_REGION
