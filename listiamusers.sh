# Get list of AWS regions
 regions=$(aws ec2 describe-regions --output json | jq -r '.Regions[].RegionName')
#
# # Loop through each region
 for region in $regions; do
     echo "Region: $region"
         aws iam list-users --region $region --output table
         done