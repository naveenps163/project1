#!/bin/bash

# Function to list IAM users
list_users() {
    echo "IAM Users:"
    aws iam list-users --output text --query 'Users[*].[UserName, UserId, Arn]' | \
    awk 'BEGIN {
        printf "| %-20s | %-30s | %-70s |\n", "Username", "User ID", "ARN"
        printf "|----------------------+--------------------------------+-----------------------------------------------------------------------|\n"
    }
    {
        printf "| %-20s | %-30s | %-70s |\n", $1, $2, $3
    }'
}

# Function to delete access keys
delete_access_keys() {
    local username="$1"
    echo "Deleting access keys for IAM user: $username"
    aws iam list-access-keys --user-name "$username" --output json | jq -r '.AccessKeyMetadata[].AccessKeyId' | \
    while IFS= read -r access_key_id; do
        aws iam delete-access-key --user-name "$username" --access-key-id "$access_key_id"
        echo "Deleted access key: $access_key_id"
    done
}

# List IAM users
list_users

# Prompt for the username to delete
read -p "Enter the username of the IAM user to delete: " USERNAME

# Confirm deletion
read -p "Are you sure you want to delete the IAM user '$USERNAME'? (yes/no): " CONFIRM
if [ "$CONFIRM" != "yes" ]; then
    echo "Deletion canceled."
    exit 0
fi

# Delete access keys
delete_access_keys "$USERNAME"

# Delete IAM user
echo "Deleting IAM user: $USERNAME"
aws iam delete-user --user-name "$USERNAME"
echo "IAM user '$USERNAME' deleted successfully."

# List IAM users again to verify deletion
list_users
