#!/bin/bash

# Get a list of AWS regions
regions=$(aws ec2 describe-regions --query 'Regions[].RegionName' --output text)

# Function to list snapshots in a region
list_snapshots_in_region() {
    region=$1
    echo "Snapshots in region $region:"
    aws ec2 describe-snapshots --region $region --query 'Snapshots[*].[SnapshotId,VolumeId,StartTime,State,Description]' --output table
}

# Main function
main() {
    for region in $regions; do
        list_snapshots_in_region $region
    done
}

# Execute main function
main
