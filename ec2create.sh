#!/bin/bash

# Check if required arguments are provided
if [ $# -ne 6 ]; then
    echo "Usage: $0 <Region> <AMI_ID> <Instance_Type> <Subnet_ID> <Key_Pair_Name> <Number_of_Instances>"
    exit 1
fi

# Read input arguments
REGION=$1
AMI_ID=$2
INSTANCE_TYPE=$3
SUBNET_ID=$4
KEY_PAIR_NAME=$5
NUM_INSTANCES=$6

# Set AWS region
export AWS_DEFAULT_REGION=$REGION

# Create EC2 instances
echo "Creating $NUM_INSTANCES EC2 instances in region $REGION..."
aws ec2 run-instances \
    --image-id $AMI_ID \
    --instance-type $INSTANCE_TYPE \
    --subnet-id $SUBNET_ID \
    --key-name $KEY_PAIR_NAME \
    --count $NUM_INSTANCES \
    --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=MyEC2Instance}]'

echo "EC2 instances creation initiated."


#list of AMI Images 
#ubuntu--> ami-0f58b397bc5c1f2e8
#SUSE---> ami-059aa0510020f477b
#amazon-linux ---> ami-013e83f579886baeb

