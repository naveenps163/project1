#!/bin/bash

# Function to list all S3 buckets
list_buckets() {
    echo "Listing all S3 buckets..."
    aws s3 ls
}

# Function to create a new S3 bucket
create_bucket() {
    read -p "Enter the name of the new bucket: " bucket_name

    echo "Creating S3 bucket $bucket_name..."
    aws s3 mb s3://$bucket_name
    echo "S3 bucket $bucket_name created successfully."
}

# Function to delete an existing S3 bucket
delete_bucket() {
    read -p "Enter the name of the bucket to delete: " bucket_name

    echo "Deleting S3 bucket $bucket_name..."
    aws s3 rb s3://$bucket_name --force
    echo "S3 bucket $bucket_name deleted successfully."
}

# Main menu
while true; do
    echo "1. List all S3 buckets"
    echo "2. Create a new S3 bucket"
    echo "3. Delete an existing S3 bucket"
    echo "4. Exit"
    read -p "Select an option: " choice

    case $choice in
        1) list_buckets;;

        2) create_bucket;;
        3) delete_bucket;;
        4) exit;;
        *) echo "Invalid option. Please try again.";;
    esac
done
