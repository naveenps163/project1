#!/bin/bash

# Function to list EC2 instances
list_instances() {
    aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId, State.Name, Tags[?Key==`Name`].Value | [0]]' --output table
}

# Function to create snapshot of an instance
create_snapshot() {
    instance_id=$1
    volume_id=$(aws ec2 describe-instances --instance-ids $instance_id --query 'Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId' --output text)
    snapshot_id=$(aws ec2 create-snapshot --volume-id $volume_id --query 'SnapshotId' --output text)
    echo "Snapshot created for volume $volume_id: $snapshot_id"
}

# Main function
main() {
    echo "Listing all EC2 instances:"
    list_instances
    read -p "Enter EC2 Instance ID to take snapshot: " instance_id
    create_snapshot $instance_id
}

# Execute main function
main

