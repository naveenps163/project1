#!/bin/bash
#
## Function to display usage information
usage() {
    echo "Usage: $0 <region> <username>"
        echo "Example: $0 us-east-1 john_doe"
        }

#        # Check if region and username are provided
        if [ $# -ne 2 ]; then
            usage
                exit 1
                fi
#
#                # Assign input parameters to variables
                AWS_REGION=$1
                USER_NAME=$2
#
#                # Create IAM user
                aws iam create-user --user-name $USER_NAME --region $AWS_REGION
#
#                # Generate access key and secret key for the IAM user
                output=$(aws iam create-access-key --user-name $USER_NAME --region $AWS_REGION)
#
#                # Extract access key and secret key from output
                access_key=$(echo $output | jq -r '.AccessKey.AccessKeyId')
                secret_key=$(echo $output | jq -r '.AccessKey.SecretAccessKey')
#
                echo "IAM user $USER_NAME created successfully in region $AWS_REGION."
                echo "Access Key ID: $access_key"
                echo "Secret Access Key: $secret_key"
#