#!/bin/bash

# List all running EC2 instances and store their instance IDs and names
echo "Fetching running EC2 instances..."
instances=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query 'Reservations[*].Instances[*].[InstanceId, Tags[?Key==`Name`].Value]' --output text)

# Check if there are any running instances
if [ -z "$instances" ]; then
    echo "No running EC2 instances found."
    exit 1
fi

# Print a menu of running instances for the user to choose from
echo "Select an EC2 instance by its name:"
IFS=$'\n'       # Set the Internal Field Separator to newline
select instance in $instances; do
    if [ -n "$instance" ]; then
        instance_id=$(echo "$instance" | awk '{print $1}')
        instance_name=$(echo "$instance" | awk '{$1=""; print $0}' | xargs)
        break
    else
        echo "Invalid selection. Please try again."
    fi
done

# SSH into the selected instance and count the number of running processes
echo "SSHing into EC2 instance '$instance_name' (Instance ID: $instance_id)..."
ssh -o StrictHostKeyChecking=no ec2-user@$instance_id 'bash -s' << 'ENDSSH'
    # Use the ps command to list all running processes and count the lines
    num_processes=$(ps aux | wc -l)

    # Subtract 1 from the total count to exclude the header line
    num_processes=$((num_processes - 1))

    echo "Number of running processes on EC2 instance '$(hostname)': $num_processes"
ENDSSH
