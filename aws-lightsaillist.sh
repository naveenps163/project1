#!/bin/bash

# Function to list bundle IDs
list_bundle_ids() {
    echo "Available Bundle IDs:"
    aws lightsail get-bundles \
        --query 'bundles[*].[bundleId, name]' \
        --output table
}

# Function to list blueprint IDs
list_blueprint_ids() {
    echo ""
    echo "Available Blueprint IDs:"
    aws lightsail get-blueprints \
        --query 'blueprints[*].[blueprintId, name]' \
        --output table
}

# Function to list instances
list_instances() {
    echo ""
    echo "Existing Instances:"
    aws lightsail get-instances \
        --query 'instances[*].[name, state.name, location.availabilityZone, publicIpAddress]' \
        --output table
}

# Function to create instance
create_instance() {
    read -p "Enter region: " region
    read -p "Enter bundle ID: " bundle_id
    read -p "Enter blueprint ID: " blueprint_id
    read -p "Enter instance name: " instance_name

    # Create Lightsail instance
    aws lightsail create-instances \
        --instance-names "$instance_name" \
        --availability-zone "${region}a" \
        --bundle-id "$bundle_id" \
        --blueprint-id "$blueprint_id" \
        --output text \
        --region "$region"

    echo "Lightsail instance created in region $region with name $instance_name."
}

# Main function
main() {
    PS3="Select an option: "
    options=("List Bundle IDs" "List Blueprint IDs" "List Instances" "Create Instance" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "List Bundle IDs")
                list_bundle_ids
                ;;
            "List Blueprint IDs")
                list_blueprint_ids
                ;;
            "List Instances")
                list_instances
                ;;
            "Create Instance")
                create_instance
                ;;
            "Quit")
                break
                ;;
            *) echo "Invalid option";;
        esac
    done
}

main
