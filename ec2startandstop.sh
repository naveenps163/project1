#!/bin/bash

# List EC2 instances
list_instances() {
    aws ec2 describe-instances --region $1 --query 'Reservations[*].Instances[*].[InstanceId, State.Name]' --output table
}

# Start EC2 instance
start_instance() {
    read -p "Enter the ID of the EC2 instance to start: " INSTANCE_ID
    aws ec2 start-instances --region $1 --instance-ids $INSTANCE_ID
    echo "EC2 instance $INSTANCE_ID started."
}

# Stop EC2 instance
stop_instance() {
    read -p "Enter the ID of the EC2 instance to stop: " INSTANCE_ID
    aws ec2 stop-instances --region $1 --instance-ids $INSTANCE_ID
    echo "EC2 instance $INSTANCE_ID stopped."
}

# Main menu
show_menu() {
    echo "1. List EC2 instances"
    echo "2. Start an EC2 instance"
    echo "3. Stop an EC2 instance"
    echo "4. Exit"
}

# Main script
if [ -z "$1" ]; then
    echo "Usage: $0 <region>"
    exit 1
fi

AWS_REGION=$1

while true; do
    show_menu
    read -p "Enter your choice: " choice
    case $choice in
        1) list_instances $AWS_REGION ;;
        2) start_instance $AWS_REGION ;;
        3) stop_instance $AWS_REGION ;;
        4) echo "Exiting." && exit 0 ;;
        *) echo "Invalid choice. Please enter a number between 1 and 4." ;;
    esac
    echo ""
done
