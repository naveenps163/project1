#!/bin/bash

# Check if required arguments are provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <Region>"
    exit 1
fi

# Read input argument
REGION=$1

# Set AWS region
export AWS_DEFAULT_REGION=$REGION

# Get list of EC2 instances
echo "Fetching list of EC2 instances in region $REGION..."
INSTANCE_LIST=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId,Tags[?Key==`Name`].Value | [0],State.Name]' --output text)

# Check if there are instances
if [ -z "$INSTANCE_LIST" ]; then
    echo "No EC2 instances found in region $REGION."
    exit 0
fi

# Print list of instances
echo "List of EC2 instances in region $REGION:"
echo "$INSTANCE_LIST" | awk '{printf "%s. Instance ID: %s, Name: %s, State: %s\n", NR, $1, $2, $3}'

# Prompt user to select instance for deletion
read -p "Enter the number of the instance to delete: " INSTANCE_NUM

# Validate user input
if ! [[ "$INSTANCE_NUM" =~ ^[0-9]+$ ]] || [ "$INSTANCE_NUM" -lt 1 ] || [ "$INSTANCE_NUM" -gt $(echo "$INSTANCE_LIST" | wc -l) ]; then
    echo "Invalid input. Please enter a valid number."
    exit 1
fi

# Extract instance ID from user's selection
INSTANCE_ID=$(echo "$INSTANCE_LIST" | sed -n "${INSTANCE_NUM}p" | awk '{print $1}')

# Terminate selected EC2 instance
echo "Terminating EC2 instance $INSTANCE_ID in region $REGION..."
aws ec2 terminate-instances --instance-ids $INSTANCE_ID

echo "EC2 instance termination initiated."

#command to run the script 
#./ec2delete.sh ap-south-1
