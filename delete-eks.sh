#!/bin/bash

# Check if both cluster name and region are provided as arguments
if [ $# -ne 2 ]; then
    echo "Usage: $0 [cluster-name] [region]"
    exit 1
fi

CLUSTER_NAME="$1"
AWS_REGION="$2"

# Delete the specified EKS cluster
echo "Deleting EKS cluster: $CLUSTER_NAME in region: $AWS_REGION"
aws eks delete-cluster --name "$CLUSTER_NAME" --region "$AWS_REGION"
