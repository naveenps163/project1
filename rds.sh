#!/bin/bash

# Function to create an Amazon RDS cluster
create_rds_cluster() {
    local cluster_identifier="$1"
    local db_engine="$2"
    local db_engine_version="$3"
    local db_instance_class="$4"
    local master_username="$5"
    local master_password="$6"
    local db_subnet_group_name="$7"
    local availability_zones="$8"
    local region="$9"
    
    echo "Creating Amazon RDS cluster..."
    aws rds create-db-cluster --db-cluster-identifier $cluster_identifier \
        --engine $db_engine --engine-version $db_engine_version \
        --db-instance-class $db_instance_class --master-username $master_username \
        --master-user-password $master_password --db-subnet-group-name $db_subnet_group_name \
        --availability-zones $availability_zones --region $region
    echo "Amazon RDS cluster created successfully!"
}

# Main script starts here

# Set AWS Region
read -p "Enter AWS Region: " AWS_REGION

# Set RDS Cluster Identifier
read -p "Enter RDS Cluster Identifier: " CLUSTER_IDENTIFIER

# Set Database Engine
read -p "Enter Database Engine (e.g., aurora): " DB_ENGINE

# Set Database Engine Version
read -p "Enter Database Engine Version (e.g., 5.6.10a): " DB_ENGINE_VERSION

# Set Database Instance Class
read -p "Enter Database Instance Class (e.g., db.t2.micro): " DB_INSTANCE_CLASS

# Set Master Username
read -p "Enter Master Username: " MASTER_USERNAME

# Set Master Password
read -p "Enter Master Password: " MASTER_PASSWORD

# Set Database Subnet Group Name
read -p "Enter Database Subnet Group Name: " DB_SUBNET_GROUP_NAME

# Set Availability Zones
read -p "Enter Availability Zones (comma-separated list): " AVAILABILITY_ZONES

# Create Amazon RDS cluster
create_rds_cluster $CLUSTER_IDENTIFIER $DB_ENGINE $DB_ENGINE_VERSION \
    $DB_INSTANCE_CLASS $MASTER_USERNAME $MASTER_PASSWORD \
    $DB_SUBNET_GROUP_NAME $AVAILABILITY_ZONES $AWS_REGION
